package matchinglogger

import (
	"strings"

	log "github.com/sirupsen/logrus"
)

// MatchingLevelHook is used for implementing the logrus.Hook interface
type MatchingLevelHook struct{}

// Levels defines the logrus levels we support dynamically changing the log level to
func (h *MatchingLevelHook) Levels() []log.Level {
	return log.AllLevels
}

// kics uses zerolog ConsoleWriter levels https://github.com/rs/zerolog/blob/582f820/globals.go#L142-L148
var kicsToLogrusLevel = map[string]log.Level{
	"PNC": log.PanicLevel,
	"FTL": log.FatalLevel,
	"ERR": log.ErrorLevel,
	"WRN": log.WarnLevel,
	"INF": log.InfoLevel,
	"DBG": log.DebugLevel,
	"TRC": log.TraceLevel,
}

// Fire is a hook to change the logrus level at runtime to match the kics log level, based on the message contents
func (h *MatchingLevelHook) Fire(entry *log.Entry) error {
	logMessage := entry.Message

	parts := strings.SplitN(logMessage, " ", 3)

	// Make sure the message consists of at least 2 parts: `timestamp` and `log level`
	// For example: "4:24AM WRN KICS crash report disabled" consists of ["4:24AM", "WRN", "KICS crash report disabled"]
	if len(parts) < 2 {
		return nil
	}

	kicsLogLevel := parts[1]

	// dynamically adjust the logrus level to match the log level reported by kics
	if logrusLogLevel, ok := kicsToLogrusLevel[kicsLogLevel]; ok {
		entry.Level = logrusLogLevel
	}

	return nil
}
