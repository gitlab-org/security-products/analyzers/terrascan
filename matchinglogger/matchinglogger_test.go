package matchinglogger_test

import (
	"testing"

	log "github.com/sirupsen/logrus"
	"github.com/stretchr/testify/require"

	"gitlab.com/gitlab-org/security-products/analyzers/kics/matchinglogger"
)

func TestMatchingLevelHook_Fire(t *testing.T) {
	defaultLogLevel := log.InfoLevel

	tests := []struct {
		name          string
		message       string
		expectedLevel log.Level
	}{
		{
			name:          "Short message",
			message:       "Short",
			expectedLevel: defaultLogLevel,
		},
		{
			name:          "Info level message",
			message:       "4:24AM INF CPU: 4.0",
			expectedLevel: log.InfoLevel,
		},
		{
			name:          "Warning level message",
			message:       "12:24PM WRN KICS crash report disabled",
			expectedLevel: log.WarnLevel,
		},
		{
			name:          "Debug level message",
			message:       "4:24AM DBG resolver.Add()",
			expectedLevel: log.DebugLevel,
		},
		{
			name:          "Unknown level message",
			message:       "4:24AM UNK Unknown level",
			expectedLevel: defaultLogLevel,
		},
		{
			name:          "Incorrectly formatted message",
			message:       "some log message with an incorrect format",
			expectedLevel: defaultLogLevel,
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			hook := &matchinglogger.MatchingLevelHook{}
			entry := &log.Entry{
				Message: tt.message,
				Level:   log.InfoLevel,
			}

			err := hook.Fire(entry)
			require.NoError(t, err)

			require.Equal(t, tt.expectedLevel, entry.Level, "expected %q level, got %q", tt.expectedLevel, entry.Level)
		})
	}
}
