package main

import (
	"fmt"
	"io"
	"os"
	"os/exec"
	"path"
	"path/filepath"
	"strings"
	"syscall"

	log "github.com/sirupsen/logrus"
	"github.com/urfave/cli/v2"

	"gitlab.com/gitlab-org/security-products/analyzers/kics/matchinglogger"
	"gitlab.com/gitlab-org/security-products/analyzers/kics/metadata"
	ruleset "gitlab.com/gitlab-org/security-products/analyzers/ruleset/v3"
)

const (
	outputPath = "/tmp"
	outputName = "kics"
)

var defaultKicsQueryPath = path.Join("/", "usr", "local", "bin", "assets", "queries")

func analyzeFlags() []cli.Flag {
	return []cli.Flag{}
}

func loadRulesetConfig(projectPath string) (*ruleset.Config, error) {
	// Load custom config if available
	rulesetPath := filepath.Join(projectPath, ruleset.PathSAST)
	return ruleset.Load(rulesetPath, metadata.AnalyzerID, log.StandardLogger())
}

func analyze(_ *cli.Context, projectPath string, rulesetConfig *ruleset.Config) (io.ReadCloser, error) {
	queryPath, err := getQueryPath(rulesetConfig)
	if err != nil {
		return nil, err
	}

	reportPath := fmt.Sprintf("%s/%s.sarif", outputPath, outputName)

	log.Infof("path %s", projectPath)
	cmd := exec.Command("kics", buildArgs(projectPath, queryPath)...)
	log.Debug(cmd.String())

	scannerName := metadata.ReportScanner.Name

	// dynamically adjust the logrus level to match the log level reported by kics
	log.StandardLogger().AddHook(&matchinglogger.MatchingLevelHook{})

	// log all output from the scanner command using the Writer() default `info` level
	streamingInfoLogger := log.StandardLogger().Writer()
	defer streamingInfoLogger.Close()
	// Use the streamingInfoLogger to immediately log each line that the scanner outputs as it executes
	cmd.Stderr = streamingInfoLogger
	cmd.Stdout = streamingInfoLogger

	if err := cmd.Run(); err != nil {
		exitErr, isExitErr := err.(*exec.ExitError)
		if !isExitErr {
			log.Errorf("Unknown error: %v", err)
			return nil, fmt.Errorf("%s scanner failure: %w", scannerName, err)
		}

		status, hasExitSignal := exitErr.Sys().(syscall.WaitStatus)
		if !hasExitSignal {
			log.Errorf("Couldn't get exit status; error: %v", exitErr)
			return nil, fmt.Errorf("%s scanner failure: %w", scannerName, exitErr)
		}

		exitStatus := status.ExitStatus()
		if exitStatus != 0 {
			log.Errorf("Encountered a system problem; status code: %d, error: %v",
				exitStatus, err)
			return nil, fmt.Errorf("%s scanner failure: %w", scannerName, err)
		}
	}

	return os.Open(reportPath)
}

// getQueryPath returns the path to available OPA queries
func getQueryPath(rulesetConfig *ruleset.Config) (string, error) {
	if rulesetConfig != nil && len(rulesetConfig.Passthrough) != 0 {
		targetDir, err := ruleset.ProcessPassthroughs(rulesetConfig, log.StandardLogger())
		if err != nil {
			return targetDir, err
		}
		if err := os.RemoveAll(defaultKicsQueryPath); err != nil {
			return "", err
		}
		if err := os.Rename(targetDir, defaultKicsQueryPath); err != nil {
			return "", err
		}
		// return targetDir, nil
	}

	return defaultKicsQueryPath, nil
}

func buildArgs(projectPath string, queryPath string) []string {
	return []string{
		"scan", "--ci",
		"--path", projectPath,
		"--queries-path", queryPath,
		// Disables request to https://kics.io to pull full descriptions
		"--disable-full-descriptions",
		"--disable-secrets",
		"--log-level", kicsLoglevel(),
		"--output-path", outputPath,
		"--output-name", outputName,
		"--report-formats", "sarif",
		// This tells KICS *not* to use an exit code to tell us that results were found. By default, an exit code indicates result severity. See https://docs.kics.io/latest/results/#exit_status_code.
		"--ignore-on-exit", "results",
	}
}

// kicsLogLevel returns the uppercase log level equivalent
// as extracted from logrus, except for the "warning" level
// which is reformatted for compatibility with kics.
//
// See https://gitlab.com/gitlab-org/gitlab/-/issues/356920
func kicsLoglevel() string {
	logrusLevel := log.GetLevel()
	strLevel := logrusLevel.String()
	if logrusLevel == log.WarnLevel {
		strLevel = "warn"
	}

	return strings.ToUpper(strLevel)
}
