package main

import (
	"os"

	"gitlab.com/gitlab-org/security-products/analyzers/command/v3"
	"gitlab.com/gitlab-org/security-products/analyzers/kics/metadata"
	"gitlab.com/gitlab-org/security-products/analyzers/kics/plugin"

	log "github.com/sirupsen/logrus"
)

func main() {
	// The working directory and kics' target directory are both $CI_PROJECT_DIR
	// in CI pipeline, which triggers a hang bug of the upstream kics
	// To fix this, change the working directory to "/"
	// TODO: remove this workaround when https://github.com/Checkmarx/kics/issues/7215 is closed
	if err := os.Chdir("/"); err != nil {
		log.Fatal("Error changing directory:", err)
	}

	app := command.NewApp(metadata.AnalyzerDetails)

	app.Commands = command.NewCommands(command.Config{
		Match:             plugin.Match,
		Analyze:           analyze,
		Analyzer:          metadata.AnalyzerDetails,
		AnalyzeFlags:      analyzeFlags(),
		AnalyzeAll:        true,
		Convert:           convert,
		LoadRulesetConfig: loadRulesetConfig,
		Scanner:           metadata.ReportScanner,
		ScanType:          metadata.Type,
	})

	if err := app.Run(os.Args); err != nil {
		log.Fatal(err)
	}
}
